# Flying Whale
## Dependencies:
```
nitrogen
openbox
rofi
i3lock
imagemagick
polybar
trayer
dunst
picom
```
## Installation
Before installation check !READ_IT! files in each dir if exists
You can run installation script, but even after that you have 
to manually change some things in configs all what you need to do
is mentioned inside script or in !READ_IT! file

## Theme
Whale theme in /usr/share/themes is slightly modified Numix theme
I've only changed some colors in it and only for openbox instance

## Screenshots
![screenshot1](/uploads/56e6bfad2523e0d095384719379651ec/screenshot1.png)
![screenshot2](/uploads/3e80a5614392db950c629f38997f8874/screenshot2.png)
![screenshot3](/uploads/5a493d322a76e4b6461e124f932279db/screenshot3.png)
![screenshot4](/uploads/547eaee30b6b7ea91c7a09a70f3e91ab/screenshot4.png)
## Not screenshot
There is little gif that shows work of some scripts
Here you will see script for tiling windows, also you can navigate
between them using super+alt+arrows like in tiling window managers
but. Also showed lockscreen
![notscreenshot](/uploads/d04eb60960de898c27b7ed724a72a8ab/notscreenshot.gif)

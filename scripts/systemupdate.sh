#!/bin/bash
echo -e "\n===============\nCleaning cache\n==============="
paccache -rk1
echo -e "\n============\nArch Update\n============"
sudo pacman -Syuq --noconfirm
echo -e "\n===========\nAUR Update\n==========="
yay -Syu --nodiffmenu --noeditmenu --answerupgrade y --nocombinedupgrade
echo -e "\n=======================\nCleaning useless files\n======================="
sudo pacman -Rs $(pacman -Qtdq)
echo -e "\n===============\nCleaning cache\n==============="
paccache -ruk0
read -p "Press Enter to exit ..."

#!/bin/bash

# Write here your terminal
term=alacritty 
# Place little aliases for what you want to appear in menu here
openbox="micro $HOME/.config/openbox/rc.xml"
polybar="micro $HOME/.config/polybar/config"
polybar_scripts="ranger $HOME/.config/polybar/scripts"
polybar_modules="ranger $HOME/.config/polybar/modules"
dunst="micro $HOME/.config/dunst/dunstrc"
rofi="micro $HOME/.config/rofi/config.rasi"
rofi_themes="ranger $HOME/.config/rofi/themes"
fish="micro $HOME/.config/fish/config.fish"
scripts="ranger $HOME/documents/scripts/"

# it will be easier for you if names in that array 
# match alias you wrote before
array=$(echo -e "openbox" "polybar" "fish" "scripts" "dunst" \
	"polybar_scripts" "polybar_modules" "rofi" "rofi_themes" )
choose="$(echo $array | rofi -sep " " -dmenu -i -p "Choose config" \
	-theme configs)"

if [[ -z $choose ]]
	then exit
fi

# Also do not forget to add option here
case "$choose" in
	*'openbox') $term -e $openbox;;
	*'polybar') $term -e $polybar;;
	*'polybar_scripts') $term -e $polybar_scripts;;
	*'polybar_modules') $term -e $polybar_modules;;
	*'dunst') $term -e $dunst;;
	*'rofi') $term -e $rofi;;
	*'rofi_themes') $term -e $rofi_themes;;
	*'fish') $term -e $fish;;
	*'scripts') $term -e  $scripts;;
esac


#!/bin/bash
# Set your icons here
# For more check nerdfonts.com
shutdown="襤"
reboot="ﰇ"
lock=""
suspend="鈴"
logout=""
#"""鈴""廓" - some more icons if you can play with

options="$lock\n$reboot\n$shutdown\n$logout\n$suspend"

chosen=$(echo -e $options | rofi -dmenu -theme powermenu.rasi -selected-row 2)
if [[ -z $chosen ]]
	then exit
fi
case $chosen in
	$shutdown) 		poweroff;;
	$reboot) 		reboot;;
	$lock) 			$HOME/documents/scripts/lockscreen.sh;;
	$suspend) 		systemctl hibernate;;
	$logout) 		openbox --exit;;
esac

#!/bin/bash
# As with trayer, we getiing pid of our polybar instance here
# so we can kill it later
# You have to manually put bar name here, spawn it once, than
# use xprop to get it
u=$(xprop -name "polybar-yes_DP-2" _NET_WM_PID | grep -o '[[:digit:]]*')
if [ $u -Z ]
	then polybar yes &
	else kill $u
fi

#!/bin/bash
# here we identify pid of trayer
u=$(xprop -name "trayer" _NET_WM_PID | grep -o '[[:digit:]]*') 
if [ $u -Z ]
	then trayer --edge top \
		--align right --distancefrom right --distance 180 \
		--width 5% --alpha 0 --transparent true \
		--height 23 --tint 0x292524 &
		sleep 7
		kill $u
	else kill $u
fi
